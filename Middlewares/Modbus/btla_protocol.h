#ifndef __BTLA_PROTOCOL__
#define __BTLA_PROTOCOL__

#include <stdbool.h>
#include <stdint.h>
#include "Modbus.h"
// ┌────────────────────────┬───────────────────┬──────────────────────┬───────────────────────┬───────────────────────────┐
// │工作模式                │ 参数1             │ 参数2                │ 参数3                 │ 参数4                     │
// ├────────┬───────────────┼───────────────────┼──────────────────────┼───────────────────────┼───────────────────────────┤
// │ 值     │ 含义          │ 4bytes            │ 4bytes               │ 4bytes               │ 4bytes                    │
// │ 0x02   │ DC恒压        │ 电压设定(0.001V)  │
// │ 0x08   │ DC恒压限流    │ 电压设定(0.001V)  │ 最大充电电流(0.001A)  │ 最大放电电流(0.001A)  │
// │ 0x21   │ DC恒流        │ 电流设定(0.001A)  │
// │ 0x22   │ DC恒功率      │ 功率设定(0.001W)  │
// │ 0x23   │ DC恒阻        │ 电阻设定(0.001Ω)  │
// │ 0x24   │ DC斜坡电流    │ 起始电流(0.001A)  │ 终止电流(0.001A)      │ 时间(0.001s)          │
// │ 0x25   │ DC斜坡功率    │ 起始功率(0.001W)  │ 终止功率(0.001W)      │ 时间(0.001s)          │
// │ 0x26   │ DC恒倍率      │ 倍率设定(0.001)   │ 注意：此模式依赖电池容量，运行前需要先设置电池容量                        │
// │ 0x27   │ DC斜坡电压    │ 起始电压(0.001V)  │ 终止电压(0.001V)      │ 时间(0.001s)          │
// │ 0x28   │ DC脉冲电流    │ 电流设定1(0.001A) │ 电流设定2(0.001A)     │ 周期(0.01s)           │ 电流设定1占空比(0.01%)    │
// │ 0x29   │ DCCCCV        │ 电压设定(0.001V)  │ 电流设定(0.001A)      │ 截止电流（0.001A）    │
// │ 0x2a   │ DC脉冲电阻    │ 电阻设定1(0.001Ω) │ 电阻设定2(0.001Ω)     │ 周期(0.01s)           │ 电阻设定1占空比(0.01%)    │
// │ 0x2b   │ DC脉冲功率    │ 功率设定1(0.001W) │ 功率设定2(0.001W)     │ 周期(0.01s)           │ 功率设定1占空比(0.01%)    │
// │ 0x2c   │ DC内阻测试    │ 电流设定(0.001A)  │ 时间1(0.001s)         │ 时间2(0.001s)         │ 时间3(0.001s)             │
// │ 0x40   │ Ac恒功率      │ 有功设定(0.001W)  │ 无功设定(0.001Var)    │
// │ 0x41   │ 独立逆变      │ 逆变电压(0.001V)  │ 逆变频率(0.001Hz)     │
// │ 0x61   │ DC脉冲电压    │ 电压设定1(0.001V) │ 电压设定2(0.001V)     │ 周期(0.01s)           │ 电压设定1占空比(0.01%)    │
// │ 0x91   │ 静置          │
// │ 0x94   │ 待机          │
// └────────┴───────────────┘
typedef struct
{
    uint16_t :3;
    uint16_t short_resting:1;//短静置
    uint16_t shutdown:1;//停机
    uint16_t fault:1;//故障
    uint16_t :3;
    uint16_t soft_start:1;//软起
    uint16_t constant_voltage_op:1;//恒压运行
    uint16_t constant_current_op:1;//恒流运行
    uint16_t standby:1;//待机
    uint16_t off_grid_op:1;//离网逆变运行
    uint16_t :1;
    uint16_t AC_constant_power_op:1;//AC恒功率运行
}dev_status_t;

typedef struct
{
    uint16_t Y;//年
    uint16_t M;//月
    uint16_t D;//日
    uint16_t H;//时
    uint16_t Min;//分
    uint16_t S;//秒
    uint16_t MS;//毫秒
}dev_date_t;

typedef struct
{
    uint8_t byte2;
    uint8_t byte1;
    uint8_t byte4;
    uint8_t byte3;
}dev_IP_t;

typedef struct
{
    uint32_t op_mode;
    int32_t para1;
    int32_t para2;
    int32_t para3;
    int32_t para4;
}dev_operation_mode_t;

/*------------------------Unpack and Get Data------------------------*/
//返回指针的函数可以之执行一次，然后通过指针访问存储数据
//返回值的函数需要在每次使用数据之前读取数据
//--------------From reg1001_1009(R)
dev_status_t* get_status();//设备状态
uint16_t* get_alarm_code();//告警代码
uint16_t* get_fault_code();//故障代码
int32_t get_DC_0_001V();//直流电压 0.001V
int32_t get_DC_0_001A();//直流电流 0.001A
int32_t get_DC_W();//直流功率 W
//--------------From reg1010_101B(R)
uint32_t get_in_mode_0_001Wh();//当前工作模式累计Wh 0.001Wh
uint32_t get_charge_0_001Wh();//充电累计Wh 0.001Wh
uint32_t get_discharge_0_001Wh();//放电累计Wh
//--------------From reg1024_1071(R)
uint16_t* get_batt_status();//电池状态
uint16_t* get_batt_minor_alarm();//电池轻度告警
uint16_t* get_batt_moderate_alarm();//电池中度告警
uint16_t* get_batt_severe_alarm();//电池重度告警
uint16_t* get_batt_max_charge_0_1A();//电池组最大允许充电电流 0.1A
uint16_t* get_batt_max_discharge_0_1A();//电池组最大允许放电电流 0.1A
uint16_t* get_batt_max_charge_100W();//电池组最大允许充电功率 0.1kW
uint16_t* get_batt_max_discharge_100W();//电池组最大允许放电功率 0.1kW
uint16_t* get_BMS_0_1V();//BMS电压 0.1V
int16_t* get_BMS_0_1A();//BMS电流 0.1A
uint16_t* get_BMS_SOC_0_01pct();//BMSSOC 0.01%
uint16_t* get_BMS_SOH_0_01pct();//BMSSOH 0.01%
uint32_t get_grid_0_001V();//电网U相电压（单相设备只有U相） 0.001V
uint32_t get_grid_0_001A();//电网U相电流（单相设备只有U相） 0.001V
int32_t get_sys_W();//系统有功功率 W
int32_t get_sys_Var();//系统无功功率 int32 Var
uint32_t get_sys_VA();//系统视在功率 uint32 VA
int16_t* get_0_001PF();//PF int16 0.001
uint16_t* get_0_01Hz();//频率 uint16 0.01Hz
//--------------From reg1119(R)
uint16_t* get_bus_0_1V();//母线电压
//--------------From reg14E6_14EB(R)
uint16_t* get_op_hours();//运行时间-时uint16
uint16_t* get_op_minutes();//运行时间-分uint16
uint16_t* get_op_seconds();//运行时间-秒uint16
uint16_t* get_op_milliseconds();//运行时间-毫秒uint16

//--------------From reg2008_2035(RW)
dev_date_t* get_date();//获取日期
bool write_date(modbusHandler_t *modH,dev_date_t* date);//更新设备日期
dev_IP_t* get_IP_address();//获取设备IP地址
dev_IP_t* get_gateway_address();//获取设备网关地址
bool write_IP_address(modbusHandler_t *modH,dev_IP_t* IP);//更新设备网关地址
bool write_gateway_address(modbusHandler_t *modH,dev_IP_t* Gateway);//更新设备网关地址
dev_operation_mode_t get_operation_mode();//获取当前工作模式与参数
bool write_operation_mode(modbusHandler_t *modH,dev_operation_mode_t* mode);//更新当前工作模式
uint16_t* get_start_stop_CMD();//读取启动/停机命令
// uint16_t* get_fault_status_clear_CMD();//故障状态清除命令
uint16_t* get_self_start_flag();//开机自启动标志
bool write_start_stop_CMD(modbusHandler_t *modH,bool start);//启动/停机命令
bool write_fault_status_clear_CMD(modbusHandler_t *modH);//故障状态清除命令
bool write_self_start_flag(modbusHandler_t *modH,bool start);//开机自启动标志
uint32_t get_DC_max_0_001V();//直流最大电压
uint32_t get_DC_min_0_001V();//直流最小电压
uint32_t get_DC_max_charge_0_001A();//最大充电电流
uint32_t get_DC_max_discharge_0_001A();//最大放电电流
uint32_t get_DC_max_charge_0_001W();//最大充电功率
uint32_t get_DC_max_discharge_0_001W();//最大放电功率
bool write_DC_max_0_001V(modbusHandler_t *modH, uint32_t value);//直流最大电压
bool write_DC_min_0_001V(modbusHandler_t *modH, uint32_t value);//直流最小电压
bool write_DC_max_charge_0_001A(modbusHandler_t *modH, uint32_t value);//最大充电电流
bool write_DC_max_discharge_0_001A(modbusHandler_t *modH, uint32_t value);//最大放电电流
bool write_DC_max_charge_0_001W(modbusHandler_t *modH, uint32_t value);//最大充电功率
bool write_DC_max_discharge_0_001W(modbusHandler_t *modH, uint32_t value);//最大放电功率
uint32_t get_AC_max_0_001V();//交流电压上限
uint32_t get_AC_min_0_001V();//交流电压下限
uint32_t get_AC_max_0_001A();//交流电流上限
uint16_t* get_AC_freq_max_0_01Hz();//交流频率上限
uint16_t* get_AC_freq_min_0_01Hz();//交流频率下限
bool write_AC_max_0_001V(modbusHandler_t *modH, uint32_t value);//交流电压上限
bool write_AC_max_0_001V(modbusHandler_t *modH, uint32_t value);//交流电压下限
bool write_AC_max_0_001A(modbusHandler_t *modH, uint32_t value);//交流电流上限
bool write_AC_freq_max_0_01Hz(modbusHandler_t *modH,uint16_t value);//交流频率上限
bool write_AC_freq_min_0_01Hz(modbusHandler_t *modH,uint16_t value);//交流频率下限

//--------------From reg2042_2044(RW)
uint16_t* get_hall_ratio();

//--------------From reg2900_2906(RW)
bool write_reset_reboot_CMD(modbusHandler_t *modH,uint16_t value);//设备复位重启

/*------------------------Modbus Read Registers------------------------*/
uint8_t btla_RO_regs_read(modbusHandler_t *modH);    //通过modbus更新所有只读寄存器，任务或中断中循环执行,建议周期大于1S
uint8_t btla_all_regs_read(modbusHandler_t *modH);  //通过modbus更新所有寄存器，任务或中断中循环执行,建议周期大于1S
bool reg1001_1009_read(modbusHandler_t *modH);
bool reg1010_101B_read(modbusHandler_t *modH);
bool reg1024_1071_read(modbusHandler_t *modH);
bool reg1119_read(modbusHandler_t *modH);
bool reg111C_1124_read(modbusHandler_t *modH);
bool reg14E6_14EB_read(modbusHandler_t *modH);
bool reg2008_2035_read(modbusHandler_t *modH);
bool reg2042_2044_read(modbusHandler_t *modH);
bool reg2800_2803_read(modbusHandler_t *modH);
bool reg2900_2906_read(modbusHandler_t *modH);
bool reg2A03_2A06_read(modbusHandler_t *modH);
bool reg3003_read(modbusHandler_t *modH);

#endif /* #ifndef __BTLA_PROTOCOL__ */