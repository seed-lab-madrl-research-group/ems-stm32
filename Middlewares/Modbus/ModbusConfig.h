/*
 * ModbusConfig.h
 *
 *  Created on: Apr 28, 2021
 *      Author: Alejandro Mera
 *
 *  This is a template for the Modbus library configuration.
 *  Every project needs a tailored copy of this file renamed to ModbusConfig.h, and added to the include path.
 */

#ifndef THIRD_PARTY_MODBUS_LIB_CONFIG_MODBUSCONFIG_H_
#define THIRD_PARTY_MODBUS_LIB_CONFIG_MODBUSCONFIG_H_

/* Uncomment the following line to enable support for Modbus Monitor Print. */
// #define MONITOR_PRINT
/* Uncomment the following line to enable support for Modbus sending message Print in hex. */
// #define SEND_MSG_PRINT
/* Uncomment the following line to enable support for Modbus receiving message Print in hex. */
// #define RECV_MSG_PRINT

#define T35_M1 150               // Timer T35_M1 period for end frame detection.Modbus Device 1
#define T35_M2 150               // Timer T35_M2 period for end frame detection.Modbus Device 2
#define MAX_BUFFER 256      // Maximum size for the communication buffer in bytes.
#define TIMEOUT_MODBUS 1000 // Minimum Timeout for master query (in ticks)
#define MAX_M_HANDLERS 2    // Maximum number of modbus handlers that can work concurrently
#define MAX_TELEGRAMS 20     // Max number of Telegrams in master queue


#endif /* THIRD_PARTY_MODBUS_LIB_CONFIG_MODBUSCONFIG_H_ */
