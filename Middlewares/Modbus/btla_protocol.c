#include "btla_protocol.h"

//read-only registers
#define REG1001_1009_SIZE (9)
#define REG1010_101B_SIZE (12)
#define REG1024_1071_SIZE (78)
#define REG1119_SIZE (1)
#define REG111C_1124_SIZE (9)
#define REG14E6_14EB_SIZE (6)
//read-write registers
#define REG2008_2035_SIZE (46)
#define REG2042_2044_SIZE (3)
#define REG2800_2803_SIZE (4)
#define REG2900_2906_SIZE (7)
#define REG2A03_2A06_SIZE (4)
#define REG3003_SIZE (1)

#define OFFSET_2008_IP_ADDRESS  (7) //uint32
#define OFFSET_2008_GATEWAY     (9) //uint32
#define OFFSET_2008_OP_MODE     (11) //uint32
#define OFFSET_2008_MODE_PARA1  (13) //uint32
#define OFFSET_2008_MODE_PARA2  (15) //uint32
#define OFFSET_2008_MODE_PARA3  (17) //uint32
#define OFFSET_2008_MODE_PARA4  (19) //uint32
#define OFFSET_2008_START_STOP_CMD (21) //uint16
#define OFFSET_2008_FAULT_CLEAR_CMD (22) //uint16
#define OFFSET_2008_SELF_START_FLAG (23) //uint16
#define OFFSET_2008_DC_MAX_0_001V (24) //uint32
#define OFFSET_2008_DC_MIN_0_001V (26) //uint32
#define OFFSET_2008_DC_MAX_CHARGE_0_001A (28) //uint32
#define OFFSET_2008_DC_MAX_DISCHARGE_0_001A (30) //uint32
#define OFFSET_2008_DC_MAX_CHARGE_0_001W (32) //uint32
#define OFFSET_2008_DC_MAX_DISCHARGE_0_001W (34) //uint32
#define OFFSET_2008_AC_MAX_0_001V (36) //uint32
#define OFFSET_2008_AC_MIN_0_001V (38) //uint32
#define OFFSET_2008_AC_MAX_0_001A (40) //uint32
#define OFFSET_2008_AC_FREQ_MAX_0_01HZ (44) //uint16
#define OFFSET_2008_AC_FREQ_MIN_0_01HZ (45) //uint16

static uint16_t reg1001_1009[REG1001_1009_SIZE];
static uint16_t reg1010_101B[REG1010_101B_SIZE];
static uint16_t reg1024_1071[REG1024_1071_SIZE];
static uint16_t reg1119[REG1119_SIZE];
static uint16_t reg111C_1124[REG111C_1124_SIZE];
static uint16_t reg14E6_14EB[REG14E6_14EB_SIZE];

static uint16_t reg2008_2035[REG2008_2035_SIZE];
static uint16_t reg2042_2044[REG2042_2044_SIZE];
static uint16_t reg2800_2803[REG2800_2803_SIZE];
static uint16_t reg2900_2906[REG2900_2906_SIZE];
static uint16_t reg2A03_2A06[REG2A03_2A06_SIZE];
static uint16_t reg3003[REG3003_SIZE];

/*------------------------Unpack From reg1001_1009------------------------*/
dev_status_t* get_status(){
  return ((dev_status_t*)&(reg1001_1009));
}

uint16_t* get_alarm_code(){
    return (reg1001_1009+1);
}

uint16_t* get_fault_code(){
    return (reg1001_1009+2);
}

int32_t get_DC_0_001V(){
    bytesFields temp;
    temp.u16[0]=*(reg1001_1009+4);
    temp.u16[1]=*(reg1001_1009+3);
    return (int32_t)temp.u32;
}

int32_t get_DC_0_001A(){
    bytesFields temp;
    temp.u16[0]=*(reg1001_1009+6);
    temp.u16[1]=*(reg1001_1009+5);
    return (int32_t)temp.u32;
}

int32_t get_DC_W(){
    bytesFields temp;
    temp.u16[0]=*(reg1001_1009+8);
    temp.u16[1]=*(reg1001_1009+7);
    return (int32_t)temp.u32;
}

/*------------------------Unpack From reg1010_101B------------------------*/
uint32_t get_in_mode_0_001Wh(){
    bytesFields temp;
    temp.u16[0]=*(reg1010_101B+3);
    temp.u16[1]=*(reg1010_101B+2);
    return temp.u32;
}

uint32_t get_charge_0_001Wh(){
    bytesFields temp;
    temp.u16[0]=*(reg1010_101B+7);
    temp.u16[1]=*(reg1010_101B+6);
    return temp.u32;
}

uint32_t get_discharge_0_001Wh(){
    bytesFields temp;
    temp.u16[0]=*(reg1010_101B+11);
    temp.u16[1]=*(reg1010_101B+10);
    return temp.u32;
}

/*------------------------Unpack From reg1024_1071------------------------*/
uint16_t* get_batt_status(){
    return (reg1024_1071+0);
}

uint16_t* get_batt_minor_alarm(){
    return (reg1024_1071+1);
}

uint16_t* get_batt_moderate_alarm(){
    return (reg1024_1071+2);
}

uint16_t* get_batt_severe_alarm(){
    return (reg1024_1071+3);
}

uint16_t* get_batt_max_charge_0_1A(){
    return (reg1024_1071+4);
}

uint16_t* get_batt_max_discharge_0_1A(){
    return (reg1024_1071+5);
}

uint16_t* get_batt_max_charge_100W(){
    return (reg1024_1071+6);
}

uint16_t* get_batt_max_discharge_100W(){
    return (reg1024_1071+7);
}

uint16_t* get_BMS_0_1V(){
    return (reg1024_1071+8);
}

int16_t* get_BMS_0_1A(){
    return (int16_t*)(reg1024_1071+9);
}

uint16_t* get_BMS_SOC_0_01pct(){
    return (reg1024_1071+10);
}

uint16_t* get_BMS_SOH_0_01pct(){
    return (reg1024_1071+11);
}

uint32_t get_grid_0_001V(){
    bytesFields temp;
    temp.u16[0]=*(reg1024_1071+21);
    temp.u16[1]=*(reg1024_1071+20);
    return temp.u32;
}

uint32_t get_grid_0_001A(){
    bytesFields temp;
    temp.u16[0]=*(reg1024_1071+27);
    temp.u16[1]=*(reg1024_1071+26);
    return temp.u32;
}

int32_t get_sys_W(){
    bytesFields temp;
    temp.u16[0]=*(reg1024_1071+33);
    temp.u16[1]=*(reg1024_1071+32);
    return (int32_t)temp.u32;
}

int32_t get_sys_Var(){
    bytesFields temp;
    temp.u16[0]=*(reg1024_1071+35);
    temp.u16[1]=*(reg1024_1071+34);
    return (int32_t)temp.u32;
}

uint32_t get_sys_VA(){
    bytesFields temp;
    temp.u16[0]=*(reg1024_1071+37);
    temp.u16[1]=*(reg1024_1071+36);
    return temp.u32;
}

int16_t* get_0_001PF(){
    return (int16_t*)(reg1024_1071+56);
}

uint16_t* get_0_01Hz(){
    return (reg1024_1071+57);
}

/*------------------------Unpack From reg1119------------------------*/
uint16_t* get_bus_0_1V(){
    return (reg1119);
}

/*------------------------Unpack From reg14E6_14EB------------------------*/
uint16_t* get_op_hours(){
    return (reg14E6_14EB);
}

uint16_t* get_op_minutes(){
    return (reg14E6_14EB+1);
}

uint16_t* get_op_seconds(){
    return (reg14E6_14EB+2);
}

uint16_t* get_op_milliseconds(){
    return (reg14E6_14EB+3);
}
/*------------------------Unpack From reg2008_2035------------------------*/
dev_date_t* get_date(){
    return (dev_date_t*)(reg2008_2035);
}

bool write_date(modbusHandler_t *modH,dev_date_t* date){
    static uint16_t REG[7];
    *((dev_date_t*)REG)=*date;
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008;
    frame.u16CoilsNo=7;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

dev_IP_t* get_IP_address(){
    return (dev_IP_t*)(reg2008_2035+OFFSET_2008_IP_ADDRESS);
}

dev_IP_t* get_gateway_address(){
    return (dev_IP_t*)(reg2008_2035+OFFSET_2008_GATEWAY);
}

bool write_IP_address(modbusHandler_t *modH,dev_IP_t* IP){
    static uint16_t REG[2];
    *((dev_IP_t*)REG)=*IP;
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008+OFFSET_2008_IP_ADDRESS;
    frame.u16CoilsNo=2;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool write_gateway_address(modbusHandler_t *modH,dev_IP_t* Gateway){
    static uint16_t REG[2];
    *((dev_IP_t*)REG)=*Gateway;
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008+OFFSET_2008_GATEWAY;
    frame.u16CoilsNo=2;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

dev_operation_mode_t get_operation_mode(){
    dev_operation_mode_t mode;
    bytesFields temp;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_OP_MODE+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_OP_MODE);
    mode.op_mode=(uint32_t)temp.u32;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_MODE_PARA1+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_MODE_PARA1);
    mode.para1=(int32_t)temp.u32;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_MODE_PARA2+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_MODE_PARA2);
    mode.para2=(int32_t)temp.u32;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_MODE_PARA3+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_MODE_PARA3);
    mode.para3=(int32_t)temp.u32;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_MODE_PARA4+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_MODE_PARA4);
    mode.para4=(int32_t)temp.u32;
    return mode;
}

bool write_operation_mode(modbusHandler_t *modH,dev_operation_mode_t* mode){
    static uint16_t REG[10];
    bytesFields temp;
    temp.u32=mode->op_mode;
    REG[0]=temp.u16[1];
    REG[1]=temp.u16[0];
    temp.u32=mode->para1;
    REG[2]=temp.u16[1];
    REG[3]=temp.u16[0];
    temp.u32=mode->para2;
    REG[4]=temp.u16[1];
    REG[5]=temp.u16[0];
    temp.u32=mode->para3;
    REG[6]=temp.u16[1];
    REG[7]=temp.u16[0];
    temp.u32=mode->para4;
    REG[8]=temp.u16[1];
    REG[9]=temp.u16[0];
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008+OFFSET_2008_OP_MODE;
    frame.u16CoilsNo=10;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

uint16_t* get_start_stop_CMD(){
    return (uint16_t*)(reg2008_2035+OFFSET_2008_START_STOP_CMD);
}

// uint16_t* get_fault_status_clear_CMD(){
//     return (uint16_t*)(reg2008_2035+OFFSET_2008_FAULT_CLEAR_CMD);
// }

uint16_t* get_self_start_flag(){
    return (uint16_t*)(reg2008_2035+OFFSET_2008_SELF_START_FLAG);
}

bool write_start_stop_CMD(modbusHandler_t *modH,bool start){
    static uint16_t CMD;
    CMD=(uint16_t)start;

    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x06;
    frame.u16RegAdd=0x2008+OFFSET_2008_START_STOP_CMD;
    frame.u16CoilsNo=1;
    frame.u16reg=&CMD;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool write_fault_status_clear_CMD(modbusHandler_t *modH){
    static uint16_t CMD=1;

    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x06;
    frame.u16RegAdd=0x2008+OFFSET_2008_FAULT_CLEAR_CMD;
    frame.u16CoilsNo=1;
    frame.u16reg=&CMD;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool write_self_start_flag(modbusHandler_t *modH,bool start){
    static uint16_t CMD;
    CMD=(uint16_t)start;

    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x06;
    frame.u16RegAdd=0x2008+OFFSET_2008_SELF_START_FLAG;
    frame.u16CoilsNo=1;
    frame.u16reg=&CMD;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

uint32_t get_DC_max_0_001V(){
    bytesFields temp;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_DC_MAX_0_001V+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_DC_MAX_0_001V);
    return temp.u32;
}

uint32_t get_DC_min_0_001V(){
    bytesFields temp;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_DC_MIN_0_001V+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_DC_MIN_0_001V);
    return temp.u32;
}

uint32_t get_DC_max_charge_0_001A(){
    bytesFields temp;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_DC_MAX_CHARGE_0_001A+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_DC_MAX_CHARGE_0_001A);
    return temp.u32;
}

uint32_t get_DC_max_discharge_0_001A(){
    bytesFields temp;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_DC_MAX_DISCHARGE_0_001A+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_DC_MAX_DISCHARGE_0_001A);
    return temp.u32;
}

uint32_t get_DC_max_charge_0_001W(){
    bytesFields temp;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_DC_MAX_CHARGE_0_001W+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_DC_MAX_CHARGE_0_001W);
    return temp.u32;
}

uint32_t get_DC_max_discharge_0_001W(){
    bytesFields temp;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_DC_MAX_DISCHARGE_0_001W+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_DC_MAX_DISCHARGE_0_001W);
    return temp.u32;
}

bool write_DC_max_0_001V(modbusHandler_t *modH, uint32_t value){
    static uint16_t REG[2];
    if(value>1000000)value=1000000;
    REG[0]=((bytesFields*)&value)->u16[1];
    REG[1]=((bytesFields*)&value)->u16[0];
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008+OFFSET_2008_DC_MAX_0_001V;
    frame.u16CoilsNo=2;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool write_DC_min_0_001V(modbusHandler_t *modH, uint32_t value){
    static uint16_t REG[2];
    if(value>1000000)value=1000000;
    REG[0]=((bytesFields*)&value)->u16[1];
    REG[1]=((bytesFields*)&value)->u16[0];
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008+OFFSET_2008_DC_MIN_0_001V;
    frame.u16CoilsNo=2;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool write_DC_max_charge_0_001A(modbusHandler_t *modH, uint32_t value){
    static uint16_t REG[2];
    if(value>1000000)value=1000000;
    REG[0]=((bytesFields*)&value)->u16[1];
    REG[1]=((bytesFields*)&value)->u16[0];
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008+OFFSET_2008_DC_MAX_CHARGE_0_001A;
    frame.u16CoilsNo=2;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool write_DC_max_discharge_0_001A(modbusHandler_t *modH, uint32_t value){
    static uint16_t REG[2];
    if(value>1000000)value=1000000;
    REG[0]=((bytesFields*)&value)->u16[1];
    REG[1]=((bytesFields*)&value)->u16[0];
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008+OFFSET_2008_DC_MAX_DISCHARGE_0_001A;
    frame.u16CoilsNo=2;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool write_DC_max_charge_0_001W(modbusHandler_t *modH, uint32_t value){
    static uint16_t REG[2];
    if(value>1000000000)value=1000000000;
    REG[0]=((bytesFields*)&value)->u16[1];
    REG[1]=((bytesFields*)&value)->u16[0];
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008+OFFSET_2008_DC_MAX_CHARGE_0_001W;
    frame.u16CoilsNo=2;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool write_DC_max_discharge_0_001W(modbusHandler_t *modH, uint32_t value){
    static uint16_t REG[2];
    if(value>1000000000)value=1000000000;
    REG[0]=((bytesFields*)&value)->u16[1];
    REG[1]=((bytesFields*)&value)->u16[0];
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008+OFFSET_2008_DC_MAX_DISCHARGE_0_001W;
    frame.u16CoilsNo=2;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

uint32_t get_AC_max_0_001V(){
    bytesFields temp;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_AC_MAX_0_001V+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_AC_MAX_0_001V);
    return temp.u32;
}

uint32_t get_AC_min_0_001V(){
    bytesFields temp;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_AC_MIN_0_001V+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_AC_MIN_0_001V);
    return temp.u32;
}

uint32_t get_AC_max_0_001A(){
    bytesFields temp;
    temp.u16[0]=*(reg2008_2035+OFFSET_2008_AC_MAX_0_001A+1);
    temp.u16[1]=*(reg2008_2035+OFFSET_2008_AC_MAX_0_001A);
    return temp.u32;
}

uint16_t* get_AC_freq_max_0_01Hz(){
    return (reg2008_2035+OFFSET_2008_AC_FREQ_MAX_0_01HZ);
}

uint16_t* get_AC_freq_min_0_01Hz(){
    return (reg2008_2035+OFFSET_2008_AC_FREQ_MIN_0_01HZ);
}

bool write_AC_max_0_001V(modbusHandler_t *modH, uint32_t value){
    static uint16_t REG[2];
    if(value>300000)value=300000;
    REG[0]=((bytesFields*)&value)->u16[1];
    REG[1]=((bytesFields*)&value)->u16[0];
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008+OFFSET_2008_AC_MAX_0_001V;
    frame.u16CoilsNo=2;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool write_AC_min_0_001V(modbusHandler_t *modH, uint32_t value){
    static uint16_t REG[2];
    if(value>300000)value=300000;
    REG[0]=((bytesFields*)&value)->u16[1];
    REG[1]=((bytesFields*)&value)->u16[0];
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008+OFFSET_2008_AC_MIN_0_001V;
    frame.u16CoilsNo=2;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool write_AC_max_0_001A(modbusHandler_t *modH, uint32_t value){
    static uint16_t REG[2];
    if(value>1000000)value=1000000;
    REG[0]=((bytesFields*)&value)->u16[1];
    REG[1]=((bytesFields*)&value)->u16[0];
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x10;
    frame.u16RegAdd=0x2008+OFFSET_2008_AC_MAX_0_001A;
    frame.u16CoilsNo=2;
    frame.u16reg=REG;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool write_AC_freq_max_0_01Hz(modbusHandler_t *modH,uint16_t value){
    static uint16_t V;
    V=(uint16_t)value;

    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x06;
    frame.u16RegAdd=0x2008+OFFSET_2008_AC_FREQ_MAX_0_01HZ;
    frame.u16CoilsNo=1;
    frame.u16reg=&V;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool write_AC_freq_min_0_01Hz(modbusHandler_t *modH,uint16_t value){
    static uint16_t V;
    V=(uint16_t)value;

    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x06;
    frame.u16RegAdd=0x2008+OFFSET_2008_AC_FREQ_MIN_0_01HZ;
    frame.u16CoilsNo=1;
    frame.u16reg=&V;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}
/*------------------------Unpack From reg2042_2044------------------------*/
uint16_t* get_hall_ratio()
{
    return (reg2042_2044+2);
}


/*------------------------Unpack From reg2800_2803------------------------*/




/*------------------------Unpack From reg2900_2906------------------------*/
bool write_reset_reboot_CMD(modbusHandler_t *modH,uint16_t value){
    static uint16_t CMD;
    CMD=(uint16_t)0xAAAA;

    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x06;
    frame.u16RegAdd=0x2900+2;
    frame.u16CoilsNo=1;
    frame.u16reg=&CMD;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

/*------------------------Modbus Read Registers------------------------*/
uint8_t btla_RO_regs_read(modbusHandler_t *modH){
    static uint8_t statecode=0;
    if(statecode>=6)statecode=0;
    switch (statecode)
    {
    case 0:
        if(reg1001_1009_read(modH)){
            statecode++;
        }
        else break;
    case 1:
        if(reg1010_101B_read(modH)){
            statecode++;
        }
        else break;
    case 2:
        if(reg1024_1071_read(modH)){
            statecode++;
        }
        else break;
    case 3:
        if(reg1119_read(modH)){
            statecode++;
        }
        else break;
    case 4:
        if(reg111C_1124_read(modH)){
            statecode++;
        }
        else break;
    case 5:
        if(reg14E6_14EB_read(modH)){
            statecode++;
        }
        else break;
    default:
        break;
    }
    return statecode;
}

uint8_t btla_all_regs_read(modbusHandler_t *modH){
    volatile static uint8_t statecode=0;
    if(statecode>=12)statecode=0;
    switch (statecode)
    {
    case 0:
        if(reg1001_1009_read(modH)){
            statecode++;
        }
        else break;
    case 1:
        if(reg1010_101B_read(modH)){
            statecode++;
        }
        else break;
    case 2:
        if(reg1024_1071_read(modH)){
            statecode++;
        }
        else break;
    case 3:
        if(reg1119_read(modH)){
            statecode++;
        }
        else break;
    case 4:
        if(reg111C_1124_read(modH)){
            statecode++;
        }
        else break;
    case 5:
        if(reg14E6_14EB_read(modH)){
            statecode++;
        }
        else break;
    case 6:
        if(reg2008_2035_read(modH)){
            statecode++;
        }
        else break;
    case 7:
        if(reg2042_2044_read(modH)){
            statecode++;
        }
        else break;
    case 8:
        if(reg2800_2803_read(modH)){
            statecode++;
        }
        else break;
    case 9:
        if(reg2900_2906_read(modH)){
            statecode++;
        }
        else break;
    case 10:
        if(reg2A03_2A06_read(modH)){
            statecode++;
        }
        else break;
    case 11:
        if(reg3003_read(modH)){
            statecode++;
        }
        else break;
    default:
        break;
    }
    return statecode;
}

bool reg1001_1009_read(modbusHandler_t *modH){
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x03;
    frame.u16RegAdd=0x1001;
    frame.u16CoilsNo=REG1001_1009_SIZE;
    frame.u16reg=reg1001_1009;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool reg1010_101B_read(modbusHandler_t *modH){
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x03;
    frame.u16RegAdd=0x1010;
    frame.u16CoilsNo=REG1010_101B_SIZE;
    frame.u16reg=reg1010_101B;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool reg1024_1071_read(modbusHandler_t *modH){
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x03;
    frame.u16RegAdd=0x1024;
    frame.u16CoilsNo=REG1024_1071_SIZE;
    frame.u16reg=reg1024_1071;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool reg1119_read(modbusHandler_t *modH){
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x03;
    frame.u16RegAdd=0x1119;
    frame.u16CoilsNo=REG1119_SIZE;
    frame.u16reg=reg1119;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool reg111C_1124_read(modbusHandler_t *modH){
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x03;
    frame.u16RegAdd=0x111C;
    frame.u16CoilsNo=REG111C_1124_SIZE;
    frame.u16reg=reg111C_1124;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool reg14E6_14EB_read(modbusHandler_t *modH){
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x03;
    frame.u16RegAdd=0x14E6;
    frame.u16CoilsNo=REG14E6_14EB_SIZE;
    frame.u16reg=reg14E6_14EB;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool reg2008_2035_read(modbusHandler_t *modH){
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x03;
    frame.u16RegAdd=0x2008;
    frame.u16CoilsNo=REG2008_2035_SIZE;
    frame.u16reg=reg2008_2035;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool reg2042_2044_read(modbusHandler_t *modH){
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x03;
    frame.u16RegAdd=0x2042;
    frame.u16CoilsNo=REG2042_2044_SIZE;
    frame.u16reg=reg2042_2044;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool reg2800_2803_read(modbusHandler_t *modH){
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x03;
    frame.u16RegAdd=0x2800;
    frame.u16CoilsNo=REG2800_2803_SIZE;
    frame.u16reg=reg2800_2803;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool reg2900_2906_read(modbusHandler_t *modH){
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x03;
    frame.u16RegAdd=0x2900;
    frame.u16CoilsNo=REG2900_2906_SIZE;
    frame.u16reg=reg2900_2906;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool reg2A03_2A06_read(modbusHandler_t *modH){
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x03;
    frame.u16RegAdd=0x2A03;
    frame.u16CoilsNo=REG2A03_2A06_SIZE;
    frame.u16reg=reg2A03_2A06;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}

bool reg3003_read(modbusHandler_t *modH){
    modbus_t frame;
    frame.u8id=0x01;
    frame.u8fct=0x03;
    frame.u16RegAdd=0x3003;
    frame.u16CoilsNo=REG3003_SIZE;
    frame.u16reg=reg3003;
    if(ModbusQueryPush(modH, frame))return true;
    else return false;
}
