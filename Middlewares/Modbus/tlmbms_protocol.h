#ifndef __TLMBMS_PROTOCOL__
#define __TLMBMS_PROTOCOL__

#include <stdint.h>
#include <stdbool.h>
#include "Modbus.h"

// 从机ID
#define TLM_SLAVE_ID (0)
/*
功能地址即为寄存器地址；
1cell0：1代表第一张采集板，cell0代表第一节电芯电压
1cell1：1代表第一张采集板，cell1代表第二节电芯电压
2cell0：2代表第二张采集板，cell1代表第一节电芯电压
依次类推；
1Temp0: 1代表第一张采集板, Temp0代表第一个采集温度
1Temp1: 1代表第一张采集板, Temp1代表第二个采集温度
2Temp0: 2代表第二张采集板, Temp0代表第一个采集温度
依次类推
1MaxV：1代表第一张采集板，MaxV代表最大电压
1MinV：1代表第一张采集板，MinV代表最小电压
1MaxT：1代表第一张采集板，MaxT代表最高温度
1MinT：1代表第一张采集板，MinT代表最低温度
依次类推
*/
/*
电芯电压为无符号数据单位mV,分辨率为0.1
电芯温度为有符号数据单位为℃，分辨率为0.1
总电压为无符号数据单位为V,分辨率为0.1
电流为有符号数据单位为A,分辨率为0.1
SOC为无符号数，分辨率为1
故障代码为无符号数，分辨率为1
*/
typedef struct
{
    uint16_t level_1_overV:1;// 一级过压
    uint16_t level_2_overV:1;// 二级过压
    uint16_t level_3_overV:1;// 三级过压
    
    uint16_t level_1_underV:1;// 一级欠压
    uint16_t level_2_underV:1;// 二级欠压
    uint16_t level_3_underV:1;// 三级欠压

    uint16_t level_1_overT:1;// 一级过温度
    uint16_t level_2_overT:1;// 二级过温度
    uint16_t level_3_overT:1;// 三级过温度

    uint16_t level_1_underT:1;// 一级欠温度
    uint16_t level_2_underT:1;// 一级欠温度
    uint16_t level_3_underT:1;// 一级欠温度
    
    uint16_t level_1_overA:1;// 一级过流
    uint16_t A_acquisition_fault:1;//电流采集故障
    uint16_t CAN_all_interruption_fault:1;//CAN通讯全中断故障
    uint16_t :1;
}bms_fault_code_1_t;

typedef struct
{
    uint16_t Relay_fault:1;//继电器故障
    uint16_t :1;//
    uint16_t CAN_partial_interruption_fault:1;//CAN通讯部分中断
    uint16_t Total_V_acquisition_not_match:1;//
    uint16_t :1;//
    uint16_t :1;//
    uint16_t :1;//
    uint16_t A_limit_anomal:1;//限流异常
    uint16_t level_2_overA:1;// 二级过流
    uint16_t level_3_overA:1;// 三级过流
    uint16_t overall_V_1_level_higher:1;//整体电压高一级
    uint16_t overall_V_2_level_higher:1;//整体电压高二级
    uint16_t overall_V_3_level_higher:1;//整体电压高三级
    uint16_t overall_V_1_level_lowwer:1;//整体电压低一级
    uint16_t overall_V_2_level_lowwer:1;//整体电压低二级
    uint16_t overall_V_3_level_lowwer:1;//整体电压低三级
}bms_fault_code_2_t;

typedef struct
{
    uint16_t V_differential_1_level_higher:1;//压差高一级
    uint16_t V_differential_2_level_higher:1;//压差高二级
    uint16_t V_differential_3_level_higher:1;//压差高三级
    uint16_t T_differential_1_level_lowwer:1;//温差低一级
    uint16_t T_differential_2_level_lowwer:1;//温差低二级
    uint16_t T_differential_3_level_lowwer:1;//温差低三级
    uint16_t negative_feedback_anomal:1;//负反馈异常
    uint16_t pre_charge_fault:1;//预充故障
    uint16_t positive_feedback_fault:1;//正反馈故障
    uint16_t multiple_faults:1;//多次故障
    uint16_t level_4_underV:1;//四级欠压
    uint16_t :1;//
    uint16_t :1;//
    uint16_t :1;//
    uint16_t cell_V_acquisition_fault:1;//电芯电压采集故障
    uint16_t cell_T_acquisition_fault:1;//电芯温度采集故障
}bms_fault_code_3_t;

typedef struct
{
    uint16_t charging:1;//充电	
    uint16_t discharging:1;//放电	
    uint16_t standby:1;//待机	
    uint16_t full:1;//充满	
    uint16_t empty:1;//放空
    uint16_t :3;//
    uint16_t :8;//
}bms_statu_t;

// get data from local memory functions
// uint16_t* get_bms_n_cell_m_0_0001V(uint8_t n,uint8_t m);
// int16_t* get_bms_n_cell_m_0_1T(uint8_t n,uint8_t m);
// uint16_t* get_bms_n_cell_max_0_0001V(uint8_t n);
// uint16_t* get_bms_n_cell_m_min_0_0001V(uint8_t n);
// int16_t* get_bms_n_cell_m_max_0_1T(uint8_t n);
// int16_t* get_bms_n_cell_m_min_0_1T(uint8_t n);
uint16_t* get_bms_total_0_1V_1();
int16_t* get_bms_total_0_1A_1();
uint16_t* get_bms_SOC();
bms_fault_code_1_t* get_bms_Fault_Code_1();
bms_fault_code_2_t* get_bms_Fault_Code_2();
bms_fault_code_3_t* get_bms_Fault_Code_3();
uint16_t* get_bms_max_0_0001V();
uint16_t* get_bms_min_0_0001V();
int16_t* get_bms_max_0_1T();
int16_t* get_bms_min_0_1T();
uint16_t* get_bms_total_0_1V_2();
int16_t* get_bms_total_0_1A_2();
bms_statu_t* get_bms_status();

// REG read functions
uint8_t tlmbms_RO_regs_read(modbusHandler_t *modH);
bool bmsreg0000_007C_read(modbusHandler_t *modH);
bool bmsreg007D_00F9_read(modbusHandler_t *modH);
bool bmsreg00FA_0176_read(modbusHandler_t *modH);
bool bmsreg0177_01F3_read(modbusHandler_t *modH);
bool bmsreg01F4_021B_read(modbusHandler_t *modH);
bool bmsreg05F4_065F_read(modbusHandler_t *modH);
bool bmsreg03E8_03F1_read(modbusHandler_t *modH);
bool bmsreg0484_0485_read(modbusHandler_t *modH);
bool bmsreg047D_read(modbusHandler_t *modH);
#endif /* #ifndef __TLMBMS_PROTOCOL__ */