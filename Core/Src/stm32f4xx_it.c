/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include "Modbus.h"
#include "task_modbus.h"
#include "usbd_cdc_if.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */
#define PRINT_BUF_SIZE (1024)
/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
typedef struct
{
uint8_t uxBuffer[PRINT_BUF_SIZE];
uint8_t u8start;
uint8_t u8end;
uint16_t u16available;
} RingBuffer_t;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
static RingBuffer_t Modbus1_Buf;
static RingBuffer_t Modbus2_Buf;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */
static void USART_Callback(USART_TypeDef * port);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
bool RingAdd(RingBuffer_t *xRingBuffer, uint8_t u8Val, USART_TypeDef * usart)
{
	if (xRingBuffer->u16available >= PRINT_BUF_SIZE)
	{
    return false;
	}
	else
	{
    xRingBuffer->uxBuffer[xRingBuffer->u8end] = u8Val;
    xRingBuffer->u8end = (xRingBuffer->u8end + 1) % PRINT_BUF_SIZE;
		xRingBuffer->u16available++;
    return true;
	}
}

void RingSend(RingBuffer_t *xRingBuffer,USART_TypeDef * usart){
  if(xRingBuffer->u16available){
    LL_USART_TransmitData8(usart,xRingBuffer->uxBuffer[xRingBuffer->u8start]);
		xRingBuffer->u8start = (xRingBuffer->u8start + 1) % PRINT_BUF_SIZE;
    xRingBuffer->u16available--;
    if(xRingBuffer->u16available==0){
      LL_USART_DisableIT_TC(usart);
    }
  }
  else{
    LL_USART_DisableIT_TC(usart);
  }
}

// #pragma import(__use_no_semihosting)
struct FILE
{
  int handle;
};

FILE __stdout;

void _sys_exit(int x)
{
  x=x;
}

int fputc(int ch, FILE* stream)
{
  while(!LL_USART_IsActiveFlag_TXE(PRINTF_USART));
  LL_USART_TransmitData8(PRINTF_USART,(uint8_t)ch);
  return (ch);
}

void ModbusUartSend(modbusHandler_t *modH, const uint8_t *data, uint8_t len){
  if(modH==get_modbusH_1())
  {
    for (unsigned int i = 0; i < len; i++) {
        while(!RingAdd(&Modbus1_Buf,data[i],MODBUS_1_USART));
    }
    if(!LL_USART_IsEnabledIT_TC(MODBUS_1_USART))
      LL_USART_EnableIT_TC(MODBUS_1_USART);
  }
  if(modH==get_modbusH_2())
  {
    for (unsigned int i = 0; i < len; i++) {
        while(!RingAdd(&Modbus2_Buf,data[i],MODBUS_2_USART));
    }
    if(!LL_USART_IsEnabledIT_TC(MODBUS_2_USART))
      LL_USART_EnableIT_TC(MODBUS_2_USART);
  }
}

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern PCD_HandleTypeDef hpcd_USB_OTG_HS;
extern TIM_HandleTypeDef htim10;

/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */
  while (1)
  {
  }
  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles TIM1 update interrupt and TIM10 global interrupt.
  */
void TIM1_UP_TIM10_IRQHandler(void)
{
  /* USER CODE BEGIN TIM1_UP_TIM10_IRQn 0 */

  /* USER CODE END TIM1_UP_TIM10_IRQn 0 */
  HAL_TIM_IRQHandler(&htim10);
  /* USER CODE BEGIN TIM1_UP_TIM10_IRQn 1 */

  /* USER CODE END TIM1_UP_TIM10_IRQn 1 */
}

/**
  * @brief This function handles TIM2 global interrupt.
  */
void TIM2_IRQHandler(void)
{
  /* USER CODE BEGIN TIM2_IRQn 0 */
  if(LL_TIM_IsActiveFlag_UPDATE(TIM2)){
    ReceiveComplete(get_modbusH_1());
    LL_TIM_ClearFlag_UPDATE(TIM2);
    LL_TIM_DisableCounter(TIM2);
    LL_TIM_DisableIT_UPDATE(TIM2);
  }
  /* USER CODE END TIM2_IRQn 0 */
  /* USER CODE BEGIN TIM2_IRQn 1 */

  /* USER CODE END TIM2_IRQn 1 */
}

/**
  * @brief This function handles TIM3 global interrupt.
  */
void TIM3_IRQHandler(void)
{
  /* USER CODE BEGIN TIM3_IRQn 0 */
  if(LL_TIM_IsActiveFlag_UPDATE(TIM3)){
    ReceiveComplete(get_modbusH_2());
    LL_TIM_ClearFlag_UPDATE(TIM3);
    LL_TIM_DisableCounter(TIM3);
    LL_TIM_DisableIT_UPDATE(TIM3);
  }
  /* USER CODE END TIM3_IRQn 0 */
  /* USER CODE BEGIN TIM3_IRQn 1 */

  /* USER CODE END TIM3_IRQn 1 */
}

/**
  * @brief This function handles USART1 global interrupt.
  */
void USART1_IRQHandler(void)
{
  /* USER CODE BEGIN USART1_IRQn 0 */
  USART_Callback(USART1);
  /* USER CODE END USART1_IRQn 0 */
  /* USER CODE BEGIN USART1_IRQn 1 */

  /* USER CODE END USART1_IRQn 1 */
}

/**
  * @brief This function handles USART2 global interrupt.
  */
void USART2_IRQHandler(void)
{
  /* USER CODE BEGIN USART2_IRQn 0 */
  USART_Callback(USART2);
  /* USER CODE END USART2_IRQn 0 */
  /* USER CODE BEGIN USART2_IRQn 1 */

  /* USER CODE END USART2_IRQn 1 */
}

/**
  * @brief This function handles UART5 global interrupt.
  */
void UART5_IRQHandler(void)
{
  /* USER CODE BEGIN UART5_IRQn 0 */
  USART_Callback(UART5);
  /* USER CODE END UART5_IRQn 0 */
  /* USER CODE BEGIN UART5_IRQn 1 */

  /* USER CODE END UART5_IRQn 1 */
}

/**
  * @brief This function handles USART6 global interrupt.
  */
void USART6_IRQHandler(void)
{
  /* USER CODE BEGIN USART6_IRQn 0 */
  USART_Callback(USART6);
  /* USER CODE END USART6_IRQn 0 */
  /* USER CODE BEGIN USART6_IRQn 1 */

  /* USER CODE END USART6_IRQn 1 */
}

/**
  * @brief This function handles USB On The Go HS global interrupt.
  */
void OTG_HS_IRQHandler(void)
{
  /* USER CODE BEGIN OTG_HS_IRQn 0 */

  /* USER CODE END OTG_HS_IRQn 0 */
  HAL_PCD_IRQHandler(&hpcd_USB_OTG_HS);
  /* USER CODE BEGIN OTG_HS_IRQn 1 */

  /* USER CODE END OTG_HS_IRQn 1 */
}

/* USER CODE BEGIN 1 */
void USART_Callback(USART_TypeDef * port){
  if(port==MODBUS_1_USART){
    if(LL_USART_IsActiveFlag_RXNE(MODBUS_1_USART)&&LL_USART_IsEnabledIT_RXNE(MODBUS_1_USART)){
      uint8_t byte = LL_USART_ReceiveData8(MODBUS_1_USART);
      ModbusUartRecv(get_modbusH_1(), byte);
      LL_TIM_SetCounter(TIM2,0);
      if(!LL_TIM_IsEnabledIT_UPDATE(TIM2))
        LL_TIM_EnableIT_UPDATE(TIM2);
      if(!LL_TIM_IsEnabledCounter(TIM2))
        LL_TIM_EnableCounter(TIM2);
      // LL_USART_ClearFlag_RXNE(MODBUS_1_USART);
    }
    if(LL_USART_IsActiveFlag_TC(MODBUS_1_USART)&&LL_USART_IsEnabledIT_TC(MODBUS_1_USART)){
      RingSend(&Modbus1_Buf,MODBUS_1_USART);
      LL_USART_ClearFlag_TC(MODBUS_1_USART);
    }
  }
  if(port==MODBUS_2_USART){
    if(LL_USART_IsActiveFlag_RXNE(MODBUS_2_USART)&&LL_USART_IsEnabledIT_RXNE(MODBUS_2_USART)){
      uint8_t byte = LL_USART_ReceiveData8(MODBUS_2_USART);
      ModbusUartRecv(get_modbusH_2(), byte);
      LL_TIM_SetCounter(TIM3,0);
      if(!LL_TIM_IsEnabledIT_UPDATE(TIM3))
        LL_TIM_EnableIT_UPDATE(TIM3);
      if(!LL_TIM_IsEnabledCounter(TIM3))
        LL_TIM_EnableCounter(TIM3);
      // LL_USART_ClearFlag_RXNE(MODBUS_2_USART);
    }
    if(LL_USART_IsActiveFlag_TC(MODBUS_2_USART)&&LL_USART_IsEnabledIT_TC(MODBUS_2_USART)){
      RingSend(&Modbus2_Buf,MODBUS_2_USART);
      LL_USART_ClearFlag_TC(MODBUS_2_USART);
    }
  }
}
/* USER CODE END 1 */
