#ifndef __TASK_MODBUS_H__
#define __TASK_MODBUS_H__

#include <stdint.h>
#include "Modbus.h"
/*------------------------Config------------------------*/
#define MODBUS_MSG_BUF_MAX     256


/*------------------------Function------------------------*/
void taskModbusInit();

modbusHandler_t* get_modbusH_1();

modbusHandler_t* get_modbusH_2();

#endif /* #ifndef __TASK_MODBUS_H__ */