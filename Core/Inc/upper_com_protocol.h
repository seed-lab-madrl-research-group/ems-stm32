/*----------v0.0----------*/
#ifndef __UPPER_COM_PROTOCOL__
#define __UPPER_COM_PROTOCOL__

#include <stdint.h>

typedef struct
{
    int32_t dc_0_001V;
    int32_t dc_0_001A;
    int32_t dc_W;

    uint32_t ac_0_001V;
    uint32_t ac_0_001A;

    int32_t  ac_W;
    int32_t ac_Var;
    
    uint16_t ac_0_01Hz;
    uint16_t bms_SOC;
    uint16_t bms_max_0_0001V;
    uint16_t bms_min_0_0001V;
    uint16_t bms_max_0_1T;
    uint16_t bms_min_0_1T;
}upper_com_t;


void upper_com_receive(uint8_t* Buf, uint32_t Len);
void upper_com_transmit();


#endif /* #ifndef __UPPER_COM_PROTOCOL__ */